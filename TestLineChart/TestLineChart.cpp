#include "TestLineChart.h"

TestLineChart::TestLineChart(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);


	cLayout.setObjectName(QStringLiteral("tab_"));
	cLayout.setIndex(0);
	//cLayout.setLineChart(nullptr);
	cLayout.newDiagramWidget(1, 0);//2个参数

	cLayout.show();
}

double getRandData(int min, int max)
{
	if (min==max)
	{
		max *= 2;
	}
	double m1 = (double)(rand() % 101) / 101;                        // 计算 0，1之间的随机小数,得到的值域近似为(0,1)
	min++;                                                                             //将 区间变为(min+1,max),
	double m2 = (double)((rand() % (max - min + 1)) + min);    //计算 min+1,max 之间的随机整数，得到的值域为[min+1,max]
	m2 = m2 - 1;                                                                        //令值域为[min,max-1]
	return m1 + m2;                                                                //返回值域为(min,max),为所求随机浮点数
}

void TestLineChart::on_pushButton_2_clicked(bool checked)
{
	int up = ui.spinBox_up->value();
	int next = ui.spinBox_next->value();
	for (size_t i = 0; i < 200; i++)
	{
		cLayout.addDataX(getRandData(next, up));
	}
}
