#include "DiagramWidget.h"
#include <QPainter>
#include <QtMath>
#include <QDebug>
#include "qmatrix4x4.h"

#define INTERVAL_NUMBER 20.0
#define SCALE_NUMBER 2.0

DiagramWidget::DiagramWidget(QWidget *parent)
    : QWidget(parent)
{
	_lineColor = QColor(38, 114, 179);
    m_bgStartColor = QColor(235, 235, 235);
    m_bgEndColor = QColor(215, 215, 215);
	m_pointColor = QColor(Qt::magenta);
	m_textColor = QColor(Qt::blue);

    m_chartLeftMargin = 90.0;
    m_chartTopMargin = 20.0;
    m_chartRightMargin = 30.0;
    m_chartBottomMargin = 50.0;
	m_showPointBg = true;

    this->startTimer(100);
}

DiagramWidget::~DiagramWidget()
{
}

//x=0.06 y=1.2
void DiagramWidget::init_number()
{
	m_x_min = 0.0;   //x起始值
	m_x_max = 2000.0; //x最大值
	m_stepH = 100.0;  //x间隔
}

void DiagramWidget::setBackgroundStartColor(QColor color)
{
    if(color != m_bgStartColor)
    {
        m_bgStartColor = color;
    }
}

void DiagramWidget::setBackgroundEndColor(QColor color)
{
    if(color != m_bgEndColor)
    {
        m_bgEndColor = color;
    }
}

void DiagramWidget::setTextColor(QColor color)
{
    if(color != m_textColor)
    {
        m_textColor = color;
    }
}

void DiagramWidget::setPointColor(QColor color)
{
    if(color != m_pointColor)
    {
        m_pointColor = color;
    }
}

void DiagramWidget::setYMin(qreal min)
{
    m_y_min = min;
}

void DiagramWidget::setYMax(qreal max)
{
    m_y_max = max;
}

void DiagramWidget::setXMin(qreal min)
{
    setXRange(min, m_x_max);
}

void DiagramWidget::setXMax(qreal max)
{
    setXRange(m_x_min, max);
}

void DiagramWidget::setStepHoriginal(qreal val)
{
    if(val != m_stepH)
    {
        m_stepH = val;
    }
}

void DiagramWidget::setStepVertical(qreal val)
{
    if(val != m_stepV)
    {
        m_stepV = val;
    }
}

void DiagramWidget::setMaxValue(qreal val)
{
	if (val != _max_num)
	{
		_max_num = val;
	}
}

void DiagramWidget::setChartLeftMargin(qreal margin)
{
    if(margin>=0 && margin != m_chartLeftMargin)
    {
        m_chartLeftMargin = margin;
    }
}

void DiagramWidget::setChartTopMargin(qreal margin)
{
    if(margin>=0 && margin != m_chartTopMargin)
    {
        m_chartTopMargin = margin;
    }
}

void DiagramWidget::setChartRightMargin(qreal margin)
{
    if(margin>=0 && margin != m_chartRightMargin)
    {
        m_chartRightMargin = margin;
    }
}

void DiagramWidget::setChartBottomMargin(qreal margin)
{
    if(margin>=0 && margin != m_chartBottomMargin)
    {
        m_chartBottomMargin = margin;
    }
}

QStringList DiagramWidget::DataString() const
{
    QStringList points;
    for(int i=0; i<m_points.size(); ++i)
    {
        points << QString("%1").arg(m_points[i]);
    }
    return points;
}

void DiagramWidget::addDataStr(QStringList str)
{
    bool ok;
    clearData();
    QStringList dataList = str;
    foreach(QString valStr, dataList)
    {
        if(valStr.size()<15&&"nan"!=valStr){
            double val = valStr.toDouble(&ok);
            if(ok)
            {
                m_y_max=(m_y_max>val?m_y_max:val);
                m_y_min=(m_y_min<val?m_y_min:val);
                m_points.push_back(val);
            }
        }
    }

    m_stepV =(m_y_max-m_y_min)/ 10;
    m_stepV = m_stepV<22.0?22.0:m_stepV;//间距控制范围15个以内
    setXMax(m_points.size());
    m_stepH = (m_x_max-m_x_min)/ INTERVAL_NUMBER;
}

void calculateStats(const std::vector<double>& data, double& max, double& min, double& average) {
    if (data.size() == 0) {
        max = 0;
        min = 0;
        average = 0;
        return;
    }
    double sum = accumulate(begin(data), end(data), 0.0);
    double mean = sum / data.size();
    max = *max_element(data.begin(), data.end());
    min = *min_element(data.begin(), data.end());
    average = mean;
}

double calculateStats(QVector<double>& data) {
    if (data.size() == 0) {
        return 0.0;
    }

    double sum = 0.0;
    for (size_t i = 0; i < data.size(); i++)
    {
        sum += data.at(i);
    }
    double mean = sum / data.size();
    return mean;
}

void DiagramWidget::addData(qreal val)
{
	if (_max_num)
	{
		maxNum = _max_num;
	}
	else
	{
		maxNum = qFloor((width() - m_chartLeftMargin - m_chartRightMargin) / m_stepH);
	}
	
    if(maxNum < 0) { return; }
    if(m_points.size()>maxNum)
    {
        m_points.pop_front();
    }
    m_points.push_back(val);

	setXMax(m_points.size());

    if (val && _y_max < val)
    {
        _y_max = val;
        p_y_max = val;
    }
    if (val && _y_min > val)
    {
        _y_min = val;
        p_y_min = val;
    }

	if (val && _y_max < _y_min)
	{
		val = _y_min;
		_y_min = _y_max;
		_y_max = val;
        p_y_max = _y_max;
        p_y_min = _y_min;
	}

	if (val)
	{
		_x_y_ = _y_max - _y_min;
		if (!_x_y_)
		{
			m_stepV = _y_max / INTERVAL_NUMBER;
			m_stepV = m_stepV < 0 ? -m_stepV : m_stepV;
			if (_y_max<0&& _y_min<0)
			{
				m_y_max = _y_max - m_stepV;
				m_y_min = _y_min + m_stepV;
			}
			else {
				m_y_max = _y_max + m_stepV;
				m_y_min = _y_min - m_stepV;
			}
		}
		else
		{
			m_stepV = _x_y_ / SCALE_NUMBER;
			m_y_max = _y_max + _x_y_;
			m_y_min = _y_min - _x_y_;
		}

        float zero_value = 0.0;
        if (m_y_max < 0 && m_y_min < 0)
        {
            zero_value = -(m_y_max + m_y_min);
        }
        else
        {
            zero_value = m_y_max - m_y_min;
        }

        zero_value = (height()- m_chartBottomMargin - m_chartTopMargin) / zero_value;
        zero_height = m_y_max * zero_value + m_chartTopMargin;
	}

	m_stepH = (m_x_max - m_x_min) / INTERVAL_NUMBER;
}

//设置标题
void DiagramWidget::setTitle(QString str)
{
    if(str != m_title)
    {
        m_title = str;
    }
}

//显示横线函数
void DiagramWidget::onLevelLine(bool show)
{
    if(show != m_show_level_line)
    {
        m_show_level_line = show;
    }
}

//显示竖线函数
void DiagramWidget::onVerticalLine(bool show)
{
    if(show != m_show_vertical_line)
    {
        m_show_vertical_line = show;
    }
}

//显示圆点函数
void DiagramWidget::onShowPoint(bool show)
{
    if(show != m_showPoint)
    {
        m_showPoint = show;
    }
}

//显示背景槽函数
void DiagramWidget::setShowPointBackground(bool show)
{
    if(show != m_showPointBg)
    {
        m_showPointBg = show;
    }
}

//清空数据
void DiagramWidget::clearData()
{
    if(m_points.size()>0)
    {
        m_points.clear();
		//init_number();
		_y_max = -0.99999;
        _y_min = 9999;
        p_y_max = 0.0;
        p_y_min = 0.0;
    }
}

int DiagramWidget::getIndex()
{
    return _index;
}

void DiagramWidget::setIndex(int index)
{
    _index = index;
}

void DiagramWidget::setLineChart(LineChart*chart)
{
    lchart = chart;
}

void DiagramWidget::setTitleName(QString name)
{
    titleName = name;
    init_number();
}

//重写绘制函数
void DiagramWidget::paintEvent(QPaintEvent* event)
{
    Q_UNUSED(event);
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	drawBackground(&painter);
	drawTitle(&painter);
	drawBorder(&painter);
	drawText(&painter);
	drawPoints(&painter);
}

void DiagramWidget::timerEvent(QTimerEvent* event)
{
    Q_UNUSED(event);
    this->update();
}

//布局背景颜色
void DiagramWidget::drawBackground(QPainter *p)
{
	QLinearGradient linearGradient(QPointF(0.0, 0.0), QPointF(0.0, height()));
	linearGradient.setColorAt(0.0, m_bgStartColor);
	linearGradient.setColorAt(1.0, m_bgEndColor);
	p->setPen(Qt::NoPen);
	p->setBrush(linearGradient);
	p->drawRect(rect());
}

//标题布局
void DiagramWidget::drawTitle(QPainter *p)
{
    QPointF topLeft(m_chartLeftMargin, height()- m_chartRightMargin);
    QPointF bottomRight(width()- m_chartRightMargin, height());

    /*QPointF topLeft(width()- m_chartRightMargin, 0.0);
    QPointF bottomRight(width(), height());*/

    QRectF titleRect(topLeft, bottomRight);
    p->setPen(m_textColor);
    p->drawText(titleRect, Qt::AlignLeft|Qt::AlignVCenter, QStringLiteral("最大值:%1").arg(QString::number(p_y_max, 'f', 4)));

    p->setPen(m_textColor);
    p->drawText(titleRect, Qt::AlignCenter, QStringLiteral("最小值:%1").arg(QString::number(p_y_min, 'f', 4)));

    p->setPen(m_textColor);
    p->drawText(titleRect, Qt::AlignRight| Qt::AlignVCenter, QStringLiteral("平均值:%1").arg(QString::number(calculateStats(m_points), 'f', 4)));

    QPointF topLeft1(0.0, -m_chartRightMargin);
    QPointF bottomRight1(height(), 0.0);
    QRectF titleRect1(topLeft1, bottomRight1);
    /*QFont font("Courier", 24);
    p->setFont(font);*/
    QTransform transform;
    transform.rotate(+90.0);//旋转90度
    p->setWorldTransform(transform);
    p->setPen(m_textColor);
    p->drawText(titleRect1, Qt::AlignCenter, titleName);
    transform.rotate(-90.0);//旋转90度
    p->setWorldTransform(transform);

}

//绘制曲线框
void DiagramWidget::drawBorder(QPainter *p)
{

	QPointF topLeft(m_chartLeftMargin, m_chartTopMargin);
	QPointF bottomRight(width() - m_chartRightMargin, height() - m_chartBottomMargin);
	QRectF borderRect(topLeft, bottomRight);
	//画边框 //绘制矩形,优先绘制，后绘制会覆盖之前的线绘制
	p->setPen(m_textColor);
	p->drawRect(borderRect);

	//画0线
	QPointF p1, p2;
	if (zero_height > m_chartTopMargin && zero_height < (height() - m_chartBottomMargin))
	{
		p->setPen(Qt::DotLine);
		//int hei = (height() - m_chartBottomMargin + m_chartTopMargin) / 2; //控件中心
		double dhei = borderRect.height() / (m_y_max - m_y_min) * m_y_max + m_chartTopMargin;
		p1 = QPointF(borderRect.left(), dhei);
		p2 = QPointF(borderRect.right(), dhei);
		p->drawLine(p1, p2);
	}

	//画横线条
    if(m_show_level_line)
    {
        p->setPen(Qt::DotLine);
        qreal count = (m_y_max-m_y_min)/m_stepV;
		if (count>0)
		{
			qreal stepY = borderRect.height() / count;
			qreal dy = m_chartTopMargin + stepY;//这里偏移stepY

			while (dy < borderRect.bottom())
			{
				p1 = QPointF(borderRect.left(), dy);
				p2 = QPointF(borderRect.right(), dy);
				p->drawLine(p1, p2);
				dy += stepY;
			}
		}
    }
	
	//画纵线条
    if(m_show_vertical_line)
    {
        p->setPen(Qt::DotLine);
        qreal count = (m_x_max-m_x_min)/m_stepH;
        qreal stepX = borderRect.width()/count;
        qreal dx = m_chartLeftMargin+stepX;
        while(dx<borderRect.right())
        {
            p1 = QPointF(dx,borderRect.top());
            p2 = QPointF(dx,borderRect.bottom());
            p->drawLine(p1, p2);
            dx += stepX;
        }
    }
}

//绘制刻度
void DiagramWidget::drawText(QPainter *p)
{
	QPointF topLeft(m_chartLeftMargin, m_chartTopMargin);
	QPointF bottomRight(width() - m_chartRightMargin, height() - m_chartBottomMargin);
	QRectF borderRect(topLeft, bottomRight);
	p->setPen(m_textColor);

    float textMargin = 5.0;
	QFontMetrics fm = fontMetrics();
	int textHeight = fm.height();

	//绘制纵轴刻度
	float count = (m_y_max - m_y_min) / m_stepV;
    float stepY = borderRect.height() / count;
    float dy = m_chartTopMargin;
	QPointF pos;
    float valy = m_y_max;
	while (valy >= m_y_min)
	{
		QString text = QString("%1").arg(QString::number(valy, 'f', 4));
        float textWidth = fm.width(text);
		pos = QPointF(m_chartLeftMargin - textMargin - textWidth, dy + textHeight / 2.0);
		p->drawText(pos, text);
		dy += stepY;
		valy -= m_stepV;
	}

	//绘制横轴刻度
    float countx = (m_x_max - m_x_min) / m_stepH;
    float stepX = borderRect.width() / countx;
    float dx = m_chartLeftMargin;
    float valx = m_x_min;
	while (valx <= m_x_max)
	{
		if (m_stepH == 0)
			break;
		QString text = QString("%1").arg(ceil(valx));
        float textWidth = fm.width(text);
		pos = QPointF(dx - textWidth / 2, (height() - m_chartBottomMargin + textMargin + fm.height()));
		p->drawText(pos, text);
		dx += stepX;
		valx += m_stepH;
	}
}

void DiagramWidget::drawPoints(QPainter *p)
{
    if(m_points.isEmpty())
        return;

    QVector<QPointF> posVec;
	qreal dx = 0.0;
	qreal dy = 0.0;
	//从右侧绘制第一个坐标点
	dx = width() - m_chartRightMargin;
	dy = height() - m_chartBottomMargin;
   
	qreal tempvalue = m_y_max - m_y_min;
    qreal percenty = (height() - m_chartTopMargin-m_chartBottomMargin)/(tempvalue == 0.0 ? 1.0 : tempvalue);
    qreal spacing_x = width()-m_chartRightMargin-m_chartLeftMargin;
    posVec.append(QPointF(spacing_x, dy));//结束点
    spacing_x/=(m_points.size()-1.0);
    if(dx<m_chartLeftMargin)
        return;
   
    QVector<qreal>::reverse_iterator it;
    for(it = m_points.rbegin(); it!= m_points.rend(); ++it)
    {
        posVec.append(QPointF(dx, dy-(*it-m_y_min)*percenty));
        dx -= spacing_x;
    }
    posVec.append(QPointF(dx<m_chartLeftMargin?m_chartLeftMargin:dx, dy));

    if(m_showPointBg)//不显示背景去除起始点和结束点
    {
        posVec.pop_back();
        posVec.pop_front();
    }
    
	//画圆点
    if(m_showPoint)
    {
        p->setPen(Qt::NoPen);
        p->setBrush(m_pointColor);
        for(int i=0; i<posVec.size(); ++i)
        {
            p->drawEllipse(posVec[i], 1.0, 1.0);
        }
    }
   
	//画线与填充
    p->setPen(_lineColor);
    if(m_showPointBg)
    {
        p->drawPolyline(QPolygonF(posVec));
    }
    else
    {
        p->setBrush(_lineColor);
        p->setOpacity(0.5);
        p->drawConvexPolygon(QPolygonF(posVec));
    }

}

void DiagramWidget::setYRange(qreal min, qreal max)
{
    if(min>max)
        qSwap(min, max);
    m_y_min = min;
    m_y_max = max;
}

void DiagramWidget::setXRange(qreal min, qreal max)
{
    if(min>max)
        qSwap(min, max);
    m_x_min = min;
    m_x_max = max;
}

QColor DiagramWidget::BackgroundStartColor() const { return m_bgStartColor; }

QColor DiagramWidget::BackgroundEndColor() const { return m_bgEndColor; }

QColor DiagramWidget::TextColor() const { return m_textColor; }

QColor DiagramWidget::PointColor() const { return m_pointColor; }

qreal DiagramWidget::YMin() const { return m_y_min; }

qreal DiagramWidget::YMax() const { return m_y_max; }

qreal DiagramWidget::XMin() const { return m_x_min; }

qreal DiagramWidget::XMax() const { return m_x_max; }

qreal DiagramWidget::get_points_size() const { return m_points.size(); }

qreal DiagramWidget::StepHoriginal() const { return m_stepH; }

qreal DiagramWidget::StepVertical() const { return m_stepV; }

qreal DiagramWidget::ChartLeftMargin() const { return m_chartLeftMargin; }

qreal DiagramWidget::ChartTopMargin() const { return m_chartTopMargin; }

qreal DiagramWidget::ChartRightMargin() const { return m_chartRightMargin; }

qreal DiagramWidget::ChartBottomMargin() const { return m_chartBottomMargin; }
