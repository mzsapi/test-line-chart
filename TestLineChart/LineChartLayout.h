/*
*折线图table页，自定义可绘制3参数控件，由LineChart类管理
*20210722
*author mzs
*/
#ifndef _LINE_CHART_LAYOUT_H_
#define _LINE_CHART_LAYOUT_H_

#include <QWidget>
#include "ui_LineChartLayout.h"
#include "DiagramWidget.h"

class LineChart;
class LineChartLayout : public QWidget
{
	Q_OBJECT

public:
	LineChartLayout(QWidget *parent = Q_NULLPTR);
	~LineChartLayout();

	//根据相机参数个数创建折线图窗口
	void  newDiagramWidget(int length,int camId);

	//获取折线图集合
	DiagramWidget** getDiagramWidget();

	//折线x数据追加
	void addDataX(double val);

	//折线y数据追加
	void addDataY(double val);

	//折线o数据追加
	void addDataO(double val);

	int getIndex();
	void setIndex(int);

	void setLineChart(LineChart*);

public slots:

	void on_checkBox_level_line_clicked(bool checked);
	void on_checkBox_vertical_line_clicked(bool checked);
	void on_checkBox_ellipse_clicked(bool checked);
	void on_pushButton_data_clean_clicked();

	void on_spinBox_number_valueChanged(int arg1);

private:

	Ui::LineChartLayout ui;

	LineChart *lchart=nullptr;

	DiagramWidget*diagrams[4] = {};

	int _index = 0;
};

#endif