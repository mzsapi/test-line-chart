#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_TestLineChart.h"
#include "LineChartLayout.h"

class TestLineChart : public QMainWindow
{
    Q_OBJECT

public:
    TestLineChart(QWidget *parent = Q_NULLPTR);

private slots:
	void on_pushButton_2_clicked(bool checked);

private:
    Ui::TestLineChartClass ui;

	LineChartLayout cLayout;
};
