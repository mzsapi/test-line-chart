#include "LineChartLayout.h"
#include <QVBoxLayout>


#define LINE_X 0
#define LINE_Y 1
#define LINE_O 2



LineChartLayout::LineChartLayout(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	diagrams[LINE_X] = nullptr;  //x
	diagrams[LINE_Y] = nullptr; //y 
	diagrams[LINE_O] = nullptr; //o
	diagrams[3] = nullptr;

	/*ui.checkBox_level_line->setChecked(true);
	ui.checkBox_vertical_line->setChecked(true);
	ui.checkBox_ellipse->setChecked(true);*/
}

LineChartLayout::~LineChartLayout()
{
}

void  LineChartLayout::newDiagramWidget(int length,int camId)
{
	for (size_t i = 0; i < length; i++)
	{
		DiagramWidget* verticalWidget = new DiagramWidget(this);
		verticalWidget->setObjectName(QString::fromUtf8("verticalWidget"));
		verticalWidget->setIndex(_index);
		verticalWidget->setLineChart(lchart);
		verticalWidget->setMaxValue(ui.spinBox_number->value());//传递折线最大数量
		QVBoxLayout* verticalLayout = new QVBoxLayout(verticalWidget);
		verticalLayout->setSpacing(6);
		verticalLayout->setContentsMargins(11, 11, 11, 11);
		verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
		diagrams[i] = verticalWidget;
		ui.verticalLayout_2->addWidget(verticalWidget);

		switch (i)
		{
		case 0:
			verticalWidget->setYMin(-0.1);
			verticalWidget->setYMax(0.1);
			verticalWidget->setStepVertical(0.05);
			verticalWidget->setTitleName(QStringLiteral("x偏差(mm)"));
			break;
		case 1:
			verticalWidget->setYMin(-2.0);
			verticalWidget->setYMax(2.0);
			verticalWidget->setStepVertical(0.5);
			verticalWidget->setTitleName(QStringLiteral("Y偏差(mm)"));
			break;
		case 2:
			verticalWidget->setYMin(-2.0);
			verticalWidget->setYMax(2.0);
			verticalWidget->setStepVertical(0.5);
			verticalWidget->setTitleName(QStringLiteral("面积(mm2)"));
			break;
		}
	}

	ui.verticalLayout_2->addWidget(ui.buttonWidget);
}

DiagramWidget ** LineChartLayout::getDiagramWidget()
{
	return diagrams;
}

void LineChartLayout::addDataX(double val)
{
	if (diagrams[LINE_X])
		diagrams[LINE_X]->addData(val);
}

void LineChartLayout::addDataY(double val)
{
	if (diagrams[LINE_Y])
		diagrams[LINE_Y]->addData(val);
}

void LineChartLayout::addDataO(double val)
{
	if (diagrams[LINE_O])
		diagrams[LINE_O]->addData(val);
}

int LineChartLayout::getIndex()
{
	return _index;
}

void LineChartLayout::setIndex(int index)
{
	_index = index;
}

void LineChartLayout::setLineChart(LineChart* chart)
{
	lchart = chart;
}

//关闭横线
void LineChartLayout::on_checkBox_level_line_clicked(bool checked)
{
	for (size_t i = 0; diagrams[i]!=nullptr; i++)
	{
		diagrams[i]->onLevelLine(checked);
	}
}

//关闭竖线
void LineChartLayout::on_checkBox_vertical_line_clicked(bool checked)
{
	for (size_t i = 0; diagrams[i] != nullptr; i++)
	{
		diagrams[i]->onVerticalLine(checked);
	}
}

//关闭点
void LineChartLayout::on_checkBox_ellipse_clicked(bool checked)
{
	for (size_t i = 0; diagrams[i] != nullptr; i++)
	{
		diagrams[i]->onShowPoint(checked);
	}
}

//清空数据
void LineChartLayout::on_pushButton_data_clean_clicked()
{
	for (size_t i = 0; diagrams[i] != nullptr; i++)
	{
		diagrams[i]->clearData();
	}
}

//调整数组长度
void LineChartLayout::on_spinBox_number_valueChanged(int arg1)
{
	for (size_t i = 0; diagrams[i] != nullptr; i++)
	{
		diagrams[i]->setMaxValue(arg1);
	}
}