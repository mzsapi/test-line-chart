/*
* DiagramWidget.h
*
* 折线图自定义控件类
* 源文件有详细注释。
*
*  Created on: 2018-07-29
*  Modified in: 2021-08-12
*      Author: mazs
*/

#ifndef _DIAGRAMD_WIDGET_H_
#define _DIAGRAMD_WIDGET_H_

#include <QWidget>
#include <QVector>


class LineChart;
class DiagramWidget : public QWidget
{
    Q_OBJECT

	//自定义属性
    Q_PROPERTY(QColor BackgroundStartColor READ BackgroundStartColor WRITE setBackgroundStartColor)
    Q_PROPERTY(QColor BackgroundEndColor READ BackgroundEndColor WRITE setBackgroundEndColor)
    Q_PROPERTY(QColor TextColor READ TextColor WRITE setTextColor)
    Q_PROPERTY(QColor PointColor READ PointColor WRITE setPointColor)
    Q_PROPERTY(qreal YMin READ YMin WRITE setYMin)
    Q_PROPERTY(qreal YMax READ YMax WRITE setYMax)
    Q_PROPERTY(qreal XMin READ XMin WRITE setXMin)
    Q_PROPERTY(qreal XMax READ XMax WRITE setXMax)
    Q_PROPERTY(qreal StepHoriginal READ StepHoriginal WRITE setStepHoriginal)
    Q_PROPERTY(qreal StepVertical READ StepVertical WRITE setStepVertical)
    Q_PROPERTY(qreal ChartLeftMargin READ ChartLeftMargin WRITE setChartLeftMargin)
    Q_PROPERTY(qreal ChartTopMargin READ ChartTopMargin WRITE setChartTopMargin)
    Q_PROPERTY(qreal ChartRightMargin READ ChartRightMargin WRITE setChartRightMargin)
    Q_PROPERTY(qreal ChartBottomMargin READ ChartBottomMargin WRITE setChartBottomMargin)
    Q_PROPERTY(QStringList DataString READ DataString WRITE addDataStr)

public:
    DiagramWidget(QWidget *parent = 0);
    ~DiagramWidget();

	QColor BackgroundStartColor() const;
    void setBackgroundStartColor(QColor color);
	QColor BackgroundEndColor() const;
    void setBackgroundEndColor(QColor color);
	QColor TextColor() const;
    void setTextColor(QColor color);
	QColor PointColor() const;
    void setPointColor(QColor color);
	qreal YMin() const;
    void setYMin(qreal min);
	qreal YMax() const;
    void setYMax(qreal max);
	qreal XMin() const;
    void setXMin(qreal min);
	qreal XMax() const;
    void setXMax(qreal max);
	qreal get_points_size() const;
	qreal StepHoriginal() const;
    void setStepHoriginal(qreal val);
	qreal StepVertical() const;
    void setStepVertical(qreal val);
	void setMaxValue(qreal val);
	qreal ChartLeftMargin() const;
    void setChartLeftMargin(qreal margin);
	qreal ChartTopMargin() const;
    void setChartTopMargin(qreal margin);
	qreal ChartRightMargin() const;
    void setChartRightMargin(qreal margin);
	qreal ChartBottomMargin() const;
    void setChartBottomMargin(qreal margin);
    QStringList DataString() const;
    void addDataStr(QStringList str);
    void addData(qreal val);

	void onLevelLine(bool show);
	void onVerticalLine(bool show);
	void onShowPoint(bool show);
	void clearData();

	int getIndex();
	void setIndex(int);

	void setLineChart(LineChart*);
	void setTitleName(QString);

public slots:
	void setTitle(QString str);
    void setShowPointBackground(bool show);

protected:
	//主动触发绘制
	void paintEvent(QPaintEvent*);
	//定时刷新UI
	void timerEvent(QTimerEvent* event);
    void drawBackground(QPainter* p);
    void drawTitle(QPainter* p);
    void drawBorder(QPainter* p);
    void drawText(QPainter* p);
    void drawPoints(QPainter* p);

    QSize minimumSizeHint() const { return QSize(400, 250); }

private:

    void setYRange(qreal min, qreal max);
    void setXRange(qreal min, qreal max);

	void init_number();

	QString titleName;

private:

	LineChart* lchart = nullptr;
	QColor m_bgEndColor;
	QColor m_bgStartColor;
	QColor m_textColor;
	QColor m_pointColor;
	QColor _lineColor;
	
	QString m_title;
	float m_y_min;
	float m_y_max;
	float m_x_min;
	float m_x_max;
	float m_stepH;
	float m_stepV;
	float m_chartLeftMargin;
	float m_chartTopMargin;
	float m_chartRightMargin;
	float m_chartBottomMargin;

	//table页索引
	int _index = 0;
	bool m_show_level_line = false;
	bool m_show_vertical_line = false;
	bool m_showPoint = false;
	bool m_showPointBg = false;

	QVector<qreal> m_points;

	int maxNum = 0;
	qreal _max_num = 0.0;
	qreal _y_max = -0.99999;
	qreal _y_min = 9999;
	qreal p_y_max = 0.0;
	qreal p_y_min = 0.0;
	//最大最小偏差
	qreal _x_y_ = 0.0;
	//绘制0线位置高度
	float zero_height = 0.0;
};

#endif // _DIAGRAMD_WIDGET_H_
